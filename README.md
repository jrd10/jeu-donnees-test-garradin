# Jeux de Données de tests pour Garradin

Le site de Garradin : https://garradin.eu

- Création de jeux de données de tests pour des présentations et/ou des formations sur Garradin


## Les jeux de données sur 3 niveaux

- Trois niveaux seront proposés pour le contene de la base

    - **Niveau Gestion des membres** : données de gestion des membres, des imports, des exports, la sauvegarde

    - **Niveau Comptabilité** : la comptabilité : données d'opérations d'enregistrement, des comptes, de la clôture d'un exercie

    - **Niveau Bénévolat valorisé et sites** : données pour le bénévolat valorisé, le site web, les extensions

- À valider mais à priori, un niveau de jeu de données contiendra les données des niveaux inférieurs


## À faire ...

- Le premier niveau devra être disponible pour mi-mai 2021.

- Le reste selon les besoins et ma disponiblité mais avant la rentrée de septembre 2021.

- Si vous souhaitez partager ou contribuer, n'hésitez pas me le faire savoir ici ou sur la liste de diffusion d'Aide de Garradin : https://admin.kd2.org/lists/aide@garradin.eu. Merci d'avance.

- Outils à étudier : 
    - https://pypi.org/project/prenoms/ Programme Python, nom du domaine publique
    - https://www.data.gouv.fr/fr/datasets/liste-de-prenoms-et-patronymes/
    - https://github.com/fzaninotto/Faker

## Licence

- Le présent projet est indépendant de la communauté autour de Garradin

- Dans le domaine public : CC0.  

- Voir la licence : https://gitlab.com/jrd10/jeu-donnees-test-garradin/-/blob/master/LICENSE
