# Plan de formation de Garradin pour ce jeu de données de tests


## Généralités
- Toutes les données sont fictives - Ces données sont destinées uniquement à la présentation ou à la formation de Garradin, éventuellement à des formations de logiciels équivalents en libre et open source. 

- Ces données ne peuvent pas être utilisées pour créer artificiellement des membres d'associations ou tout autre entité afin de refléter une taille de l'entité qui n'est pas la réalité.

## Module utilisateurs
- Module utilisateur pour démonstration de l'utilisation 

## Niveau Découverte : Niveau Gestion des membres
- Données de gestion des membres, des activités, des imports, des exports, la sauvegarde

- On peut imaginer, pour un même niveau, un jeu de départ et un ou plusieurs autres jeux au fur et à mesure de l'enrichissement (exemple avec des champs ajoutés dans la fiche de membre)

| Contenu de formation | Données associées |
| ------ | ------ |
| **Installation** En mode Saas, sur nom_assciation.garradin.eu. | Néant. Les autres types d'installation : YUNOHOST, local, autres, seront présentées dans le chapitre suivant |
| **Membres** | Une dizaine de membres incluant des noms d'origine culturelle différentes. Plusieurs catégories |
| **Ajouter membres** | Disposer d'une liste de quelques membres à ajouter |
| **Fiche de membres** | Champ de base + Ajouter champ pseudo + notes. Disposer |
| **Mots de passe** | Comment gérer les mots de passe ??? |
| **Infos Personnelles** | Synthèse des données membres |
| **Message collectif** | Quelques messages collectifs + modèles ??? |
| **Présentation du site** | Quelques infos. Détaillé au troisième module |
| **Activités et Cotisations** | Des adhésions + dons |
| **Tarifs** | Des tarifs étudiants, demandeurs d'emploi, autres |
| **Dons ponctuels et récurrents** | dons |
| **Virement banque, caisse** | Virement banque, rapprochement bancaire |
| **Recette** | Les exemples de cotisations et de dons, ventes de produits dérivés |
| **Dépenses** | Services bancaires (don à HelloAsso), assurance, informatique (don à Garradin) |
| **Comptes** | Comptes reflet des données ci-dessus |
| **Exercices** | Deux exercices clos |
| **Plan comptable** | Uniquement le plan comptable officiel |
 

## Niveau Comptabilité 
- la comptabilité : données d'opérations d'enregistrement, des comptes, de la clôture d'un exercie


| Contenu de formation | Données associées |
| ------ | ------ |
| **Installation** | Exemple de sites sous différents types d'installation |
| **Membres** | Catégorie, Recherches (avancées) enregistrées, Import & Export |
| **Ajouter un membre** |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |
|  |  |


## Niveau avancé : bénévolat valorisé, sites, sauvegarde et exports
- 

